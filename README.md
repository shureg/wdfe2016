# Web Design for Everybody(Basics of Web Development and Coding) Specialization #
by university of Michigan

+ Introduction to HTML5
+ Introduction to CSS3
+ Interactivity with JavaScript
+ Advanced Styling with Responsive Design

+ Web Design for Everybody Capstone



## Resources:

here you will find some links to the interesting ressourses mentioned in that courses

### HTML5 Ressourcen

* Book: [The Missing Link: An Introduction to Web Development and Programming](http://textbooks.opensuny.org/the-missing-link-an-introduction-to-web-development-and-programming/)
* [Learn HTML&CSS ](http://learn.shayhowe.com/html-css/getting-to-know-html/)
* [Web Accessibility Evaluation Tool](http://wave.webaim.org/)
* [HTML validation - article](https://www.cs.tut.fi/~jkorpela/html/validation.html)




### CSS3

+ [Css Zen Garden](http://www.csszengarden.com/)
+ [Free Web Fonts by Google](https://fonts.google.com/)



### Responsive Design

+ Article: [The Truth About Multiple H1 Tags](http://webdesign.tutsplus.com/articles/the-truth-about-multiple-h1-tags-in-the-html5-era--webdesign-16824)
+ [Media Query Examples](http://mediaqueri.es/)
+ [Responsive Design Test](http://ami.responsivedesign.is/)
+ Article: [rem vs. em vs. px](https://www.futurehosting.com/blog/web-design-basics-rem-vs-em-vs-px-sizing-elements-in-css/)
+ [rem & em examples](http://codepen.io/chriscoyier/pen/tvheK)
+ [Pixel to em](http://pxtoem.com/)
+ [Wireframe Showcase](http://www.wireframeshowcase.com/)
+ Article: [Wireframe - 10 best practices](http://www.dtelepathy.com/blog/design/learning-to-wireframe-10-best-practices)
+ [Howto read a wireframe](http://blog.fuzzymath.com/wp-content/uploads/2011/07/Fuzzy-Math-How-to-read-a-wireframe.pdf)
+ popular frameworks
     + [Bootstrap](http://getbootstrap.com/)
     + [Foundation](http://foundation.zurb.com/)
     + [Semantic UI](http://semantic-ui.com/)
     + [Pure by Yahoo](http://purecss.io/)
     + [Ulkig by YOOTheme](http://getuikit.com/)
+ Article [Tips Bootstrap CSS](https://www.sitepoint.com/responsive-web-design-tips-bootstrap-css/)


### JavaScript

+ [HTML5 pattern](http://www.html5pattern.com/)
+ Article [Hostory of debugging](http://theinstitute.ieee.org/technology-focus/technology-history/did-you-know-edison-coined-the-term-bug)
+ [CheatSheet](https://www.coursera.org/learn/javascript/supplement/i75xf/a-javascript-cheat-sheet)
+ [WOFOO Form Builder](http://www.wufoo.com/)


##General

###Web editor
+ [CodePen](http://codepen.io/)
+ [JS Bin](http://jsbin.com)
+ [JS Fiddle](https://jsfiddle.net/)

### Others

+ [W3 School](http://www.w3schools.com/html/default.asp)
+ Free Templates: [Bootstrap Zero](http://www.bootstrapzero.com/)
+ Free Templates: [Start Bootsrap](http://startbootstrap.com/)

