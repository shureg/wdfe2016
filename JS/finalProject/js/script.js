/*************************************
Or use my Code Pen solution: 
        http://codepen.io/hotfix/pen/oLwjAb
*************************************/

/* if checkbox checked, copy values on the fly*/
function completeBilligname(val) {
    var checkBox = document.getElementById("same");
    //console.log(val.id);
    //console.log(val.name);
    if (checkBox.checked) {
        var elemVal = document.getElementById(val.id).value;
        if (val.id == 'shippingZip') {
            document.getElementById("billingZip").value = elemVal;
        } else if (val.id == 'shippingName') {
            document.getElementById("billingName").value = elemVal;
        }
    }
}

/* if values where already entered, just copy values*/
function billingFunction() {
    var checkBox = document.getElementById("same");
    var billingName = document.getElementById("billingName");
    var billingZip = document.getElementById("billingZip");
    if (checkBox.checked) {
      //  console.log("is checked");
        billingName.value = document.getElementById("shippingName").value;
        billingZip.value = document.getElementById("shippingZip").value;
    } else {
        //console.log("is not checked");
        billingName.value = null;
        billingZip.value = null;
        billingZip.removeAttribute("required");
        billingName.removeAttribute("required");
    }
}